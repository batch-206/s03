import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        String[] listOfGames = {"Mario Odyssey", "Super Smash Bros. Ultimate", "Luigi's Mansion 3", "Pokemon Sword", "Pokemon Shield"};

        HashMap<String,Integer> gamesStock = new HashMap<>();
        gamesStock.put(listOfGames[0],50);
        gamesStock.put(listOfGames[1],20);
        gamesStock.put(listOfGames[2],15);
        gamesStock.put(listOfGames[3],30);
        gamesStock.put(listOfGames[4],100);
        //System.out.println(gamesStock);

        gamesStock.forEach((key,value) -> {
            System.out.println(key + " has " + value + " stocks left.");
        });

       ArrayList<String> topGames = new ArrayList<>();
         gamesStock.forEach((key,value) -> {
             if(value >= 30){
                 System.out.println(key + " has been added to the List of Top Games");
                 topGames.add(key);
             }
         });
        System.out.println("Our shop's top games:");
        System.out.println(topGames);

        boolean addItem = true;
        Scanner itemScanner = new Scanner(System.in);
        while(addItem){
            System.out.println("Would you like to add an item? Yes or No");
            String input = itemScanner.nextLine();
//            if(input == "Yes"){
//                System.out.println("Add the item");
//                String item = itemScanner.nextLine();
//
//                System.out.println("Add item stock");
//                Integer itemStock = itemScanner.nextInt();
//
//                gamesStock.put(item,itemStock);
//                addItem = false;
//            } else if (input == "No"){
//                System.out.println("Thank you.");
//                addItem = false;
//            }

            switch(input){
                case "Yes":
                    System.out.println("Add the item");
                    String item = itemScanner.nextLine();

                    System.out.println("Add item stock");
                    Integer itemStock = itemScanner.nextInt();

                    gamesStock.put(item,itemStock);
                    System.out.println(gamesStock);
                    addItem = false;
                    break;
                case "No":
                System.out.println("Thank you.");
                addItem = false;
                break;
                default:
                    System.out.println("please answer only Yes or No");
            }
        }
    }
}